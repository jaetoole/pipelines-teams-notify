# Pipelines Teams Notify

![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)

Pipelines Teams Notify is a custom Pipe created for Bitbucket Pipelines to allow notifications to be sent to your teams group. **Note**: We rely on you having an Incoming Webhook connector on your Microsoft Teams Team, if you don't have this please [click here](https://docs.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook#add-an-incoming-webhook-to-a-teams-channel).

# Usage
Using Pipelines Teams Notify is super simple! It intergrates right in to your current build steps. Under the Pipeline you want to add the pipe to, simply add the below to your scripts section
```yaml
- pipe: docker://jaetoole/pipelines-teams-notify:latest
    variables:
        WEBHOOK_URL: '<YOUR WEBHOOK URL>'
        TITLE: '<YOUR MESSAGECARD TITLE>'
        TEXT: '<YOUR MESSAGECARD TEXT>'
```

You can also:
  - Set the themecolor of the MessageCard by adding a `THEMECARD:` variable and setting it equal to a hex color value.

### Todos

 - Write Tests
 - Remove Default Debug In Favour Of Debug Flag
 - Add In Support For MessageCard Actions

### Contributing
I welcome contributions no matter how big or small! Just open a PR and send it my way!

### Buy Me A Coffee
If you like the work that I do and would like to support me, please do buy me a coffee (I feel it is the only thing that powers me somedays...)
https://www.buymeacoffee.com/jaetooledev

License
----

MIT


**Free Software, Hell Yeah!**
