#!/usr/bin/env bash

# set -e
set -x
echo "Excecuting Pipeline Teams Notify..."

THEMECOLOR=${THEMECOLOR:="0072C6"}

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"@context":"https://schema.org/extensions","@type":"MessageCard","themeColor":"'"${THEMECOLOR}"'","title":"'"${TITLE}"'","text":"'"${TEXT}"'"}' \
  ${WEBHOOK_URL}