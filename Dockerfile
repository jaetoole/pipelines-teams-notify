FROM alpine:3.9

RUN apk add --update --no-cache bash curl

COPY pipe.sh /

RUN chmod +x pipe.sh

ENTRYPOINT ["/pipe.sh"]
